const express = require('express');
const bodyParser = require('body-parser');
const ejs = require("ejs");
const Nexmo = require("nexmo");
const socketio = require("socket.io");


//Init nexmo 
const nexmo = new Nexmo({
    apiKey: 'Ton api key ',
    apiSecret: 'Ton api secret '
}, {debug: true});

//Init app
const app = express();

//Template du setup
app.set("view engine",'html');
app.engine("html",ejs.renderFile)

//Dosser public setup
app.use(express.static(__dirname + '/public'));

//Body parser middleware

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

//Index route
app.get('/', (req,res) =>{
    res.render('index')
});

//Catch from submit
app.post('/', (req,res) => {

const { number, text } = req.body;

nexmo.message.sendSms(
    'Mirage',
    number,
    text,
    {type : "unicode"},
    (err, responseData ) => {
        if(err){
            console.log(err)
        }else{
            console.dir(responseData);
            // Prend la data de reponse 
            const data = {
                id: responseData.messages[0]["message-id"],
                number: responseData.messages[0]['to']
            }

            // Emettre un mesaage au clien 
            io.emit("smsStatus", data);
        }
    }
)
})

//Defnir le port 
const port = 3000;

//Lance le server (start server)
const server = app.listen(port,() => console.log(`Serer started on ${port}`))

//Connecter a socket.io
const io = socketio(server);
io.on("connection", (socket) => {
    console.log("Connecte")
});
io.on("disconnect", () => {
    console.log("Deconnecte")
})
